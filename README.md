wclip
=====

wclip is "xclip" for Wayland

### Usage
`wclip i <mime_types>`
	Copy from stdin to the clipboard, advertising the specified MIME types to receiving programs.
`wclip o <mime_types>`
	Paste the data with one of the specified MIME types to stdout, with the one closer to the beginning of the list taking precedence.

If no MIME types are specified, the following list is used:
	- text/plain
	- TEXT
	- STRING

#### Examples
`wclip i < wclip.c` - put the wclip source code into the clipboard as text
`wclip o text/plain` - paste the clipboard data with the MIME type "text/plain" to stdout

### Caveats

Due to restrictions of the Wayland protocol, `wclip o` attempts to open a 1x1 window in order to obtain "keyboard focus". This may fail or cause disruption owning to a variety of reasons.

The clipboard data is stored in a file in `/tmp`. Attempting to copy vast amounts of data to the clipboard can have (un)intended side effects.

`wclip` forks to the background in order to handle requests to transfer data from other programs. `wclip` will automatically exit when another program takes over the clipboard.

### Compilation

Simply run the `compile` script to compile `wclip`. `gcc` and the Wayland client library are required to compile `wclip`.
